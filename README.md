# work-sel0456

## Passo 1- Clonando repositório

<ul>
  <li>Crie uma pasta em seu diretório de preferência. Caso esteja usando o VSCode, acesse a padta vazia criada, clique com o botão direito em qualquer
lugar e clique em abrir no terminal..</li>
  <li>Rode o comando code . e então o VSCode será aberto.</li>
  <li>Abra um novo terminal acessando a aba Terminal e clicando em New Terminal.</li>
  <li>Digite o comando <git clone url_do_repositorio>, onde url_do_repositorio será a url que você copiou no GitLab.</li>
  <li>Após clonar, feche o VSCode, acesse a pasta onde você clonou o projeto, abre a pasta, que será a work-sel0456 e abra o projeto no VSCode conforme o primeiro tópico. Isso vai garantir que estejamos trabalhando no mesmo diretório que foi apontado nas configurações.</li>
</ul>

## Passo 2 - Compilação
Para compilar o programa, rode o comando "make all" no terminal. Serão criados os executáveis .o.
Este projeto foi feito no VSCode. Para compilar,você também pode acessar o arquivo tasks.json, vá na aba Terminal e clique em Run Task.
Selecione a opção "build", pois o Makefile foi configurado para "build" ser o arquivo alvo.

## Passo 3 - Execução

Abra um novo terminal (bash) caso nenhum estiver aberto. Execute o comando ./main para executar o executável main.o gerado durante a compilação.

## Explicação do código

### fill_vector(float *vector, int vector_size) 

O primeiro argumento da função fill_vector é o vetor a ser preenchido. O segundo argumento é o tamanho do vetor, que deve ser um inteiro.
A função vai preencher o vetor com números consecutivos usando um loop for sobre cada elemento do vetor.

### print_vector(float *vector)

O argumento da função print_vector() é o seu vetor preenchido. Ela verifica o tamanho do vetor usando a função sizeof() e 
itera sobre o vetor, printando cada elemento em sequência.

### process_vector(float *vector)

Esta função tem como objetivo realizar algum processamento sobre o vetor construído. Neste projeto, ela apenas soma todos os elementos e divide pelo
tamanho do vetor, isto é, calcula a média de seus elementos.



