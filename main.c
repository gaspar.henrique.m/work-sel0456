#include <stdio.h>
#include "headers.h"

float vector[255] = {};

int main() {
    int size = 8;
    fill_vector(vector,size);
    print_vector(vector);
    process_vector(vector);
    
    return 0;
}
