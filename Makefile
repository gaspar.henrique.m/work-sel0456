CC = gcc
CFLAGS = -Wall -Wextra

# Diretórios
SRC_DIR = source
INC_DIR = header
BUILD_DIR = build

# Arquivos de origem (source)
SRC_FILES = $(wildcard $(SRC_DIR)/*.c)

# Arquivos de cabeçalho (header)
INC_FILES = $(wildcard $(INC_DIR)/*.h)

# Nomes dos arquivos objeto
OBJ_FILES = $(patsubst $(SRC_DIR)/%.c, $(BUILD_DIR)/%.o, $(SRC_FILES))

# Nome do programa alvo
TARGET = main

# Arquivo principal
MAIN_SRC = main.c

all: $(TARGET)

$(TARGET): $(OBJ_FILES) $(BUILD_DIR)/main.o
	$(CC) $(CFLAGS) -o $@ $^

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c $(INC_FILES)
	$(CC) $(CFLAGS) -I$(INC_DIR) -c $< -o $@

$(BUILD_DIR)/main.o: $(MAIN_SRC) $(INC_FILES)
	$(CC) $(CFLAGS) -I$(INC_DIR) -c $< -o $@

clean:
	rm -rf $(BUILD_DIR) $(TARGET)

.PHONY: all clean
