#include <stdio.h>
#include "process_vector.h"

void process_vector(float *vector)
{
    int vector_size = 0;
    float vector_mean;
    float vector_sum;
    vector_size = sizeof(vector);
    for (int i = 0; i < vector_size; i++) {
        vector_sum += vector[i];
    }
    vector_mean = vector_sum/vector_size;
    printf("\nVector mean value: %f \n",vector_mean);
}