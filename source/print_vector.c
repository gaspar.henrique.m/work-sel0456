#include <stdio.h>
#include "print_vector.h"

void print_vector(float *vector)
{
    int vector_size = 0;
    vector_size = sizeof(vector);
    printf("Vector elements: ");
    for (int i = 0; i < vector_size; i++) {
        printf("%f ", vector[i]);
    }
    printf("\n");
}
