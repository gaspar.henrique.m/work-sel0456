#include <stdio.h>
#include "fill_vector.h"

// Função para preencher o array com valores double
void fill_vector(float *vector, int vector_size) {
    for (int i = 0; i < vector_size; i++) {
        vector[i] = i;
    }
}
